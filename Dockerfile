FROM 1and1internet/ubuntu-16-apache



# sudo install

RUN \
    apt-get update && \
    apt-get install -y software-properties-common python-software-properties && \
    add-apt-repository -y -u ppa:ondrej/php && \
    apt-get update && \
    apt-get install -y imagemagick graphicsmagick && \
    apt-get install -y libapache2-mod-php7.2 php7.2-bcmath php7.2-bz2 php7.2-cli php7.2-common php7.2-curl php7.2-dba php7.2-gd php7.2-gmp php7.2-imap php7.2-intl php7.2-ldap php7.2-mbstring php7.2-mysql php7.2-odbc php7.2-pgsql php7.2-recode php7.2-snmp php7.2-soap php7.2-sqlite php7.2-tidy php7.2-xml php7.2-xmlrpc php7.2-xsl php7.2-zip && \
    apt-get install -y php-gnupg php-imagick php-mongodb php-streams php-fxsl && \
    sed -i -e 's/max_execution_time = 30/max_execution_time = 300/g' /etc/php/7.2/apache2/php.ini && \
    sed -i -e 's/upload_max_filesize = 2M/upload_max_filesize = 256M/g' /etc/php/7.2/apache2/php.ini && \
    sed -i -e 's/post_max_size = 8M/post_max_size = 512M/g' /etc/php/7.2/apache2/php.ini && \
    sed -i -e 's/memory_limit = 128M/memory_limit = 512M/g' /etc/php/7.2/apache2/php.ini && \
    sed -i -e 's/DirectoryIndex index.html index.cgi index.pl index.php index.xhtml index.htm/DirectoryIndex index.php index.html index.cgi index.pl index.xhtml index.htm/g' /etc/apache2/mods-available/dir.conf && \
    mkdir /tmp/composer/ && \
    cd /tmp/composer && \
    curl -sS https://getcomposer.org/installer | php && \
    mv composer.phar /usr/local/bin/composer && \
    chmod a+x /usr/local/bin/composer && \
    cd / && \
    rm -rf /tmp/composer && \
    apt-get remove -y python-software-properties software-properties-common && \
    apt-get autoremove -y && \
    rm -rf /var/lib/apt/lists/* && \
    chmod 777 -R /var/www && \
    apache2ctl -t && \
    mkdir -p /run /var/lib/apache2 /var/lib/php && \
    chmod -R 777 /run /var/lib/apache2 /var/lib/php /etc/php/7.2/apache2/php.ini


RUN apt-get update && \
      apt-get -y install sudo

RUN useradd -m docker && echo "docker:docker" | chpasswd && adduser docker sudo
RUN useradd -m kevin && echo "kevin:kevin" | chpasswd && adduser kevin sudo

CMD /bin/bash




# Java install
RUN apt-get update && \
apt-get install -y wget software-properties-common && \
echo oracle-java8-installer shared/accepted-oracle-license-v1-1 select true | debconf-set-selections && \
add-apt-repository -y ppa:webupd8team/java && \
apt-get update && \
apt-get install -y oracle-java8-installer 






# Node

RUN sed -e 's/^# deb /deb /g' /etc/apt/sources.list | grep "^deb " > /etc/apt/sources.list.new && \ 
mv /etc/apt/sources.list.new /etc/apt/sources.list && \ 
export DEBIAN_FRONTEND=noninteractive && \
apt-get update && \ 
apt-get install -y \
openssl \
ca-certificates \
rsyslog \
rsyslog-relp \
cron \
curl \ 
rsync \ 
logrotate \
gettext-base \
wget \ 
less \
bash \ 
bash-completion \
tar \ 
zip \
unzip \
git \
emacs24-nox \
jq figlet \
build-essential \
python \
vim && \
apt-get clean && \
rm -rf /var/lib/apt/lists/* 

RUN curl -sL https://deb.nodesource.com/setup_7.x | sudo -E bash - \ 
&& apt-get update \
&& sudo apt-get install -y nodejs
# RUN wget -qO- https://deb.nodesource.com/setup_7.x | bash - && \
 #   export DEBIAN_FRONTEND=noninteractive && \
  #  apt-get update && \
   # apt-get install -y nodejs && \ 
    #apt-get clean && \
    #rm -rf /var/lib/apt/lists/*

RUN curl "https://s3.amazonaws.com/aws-cli/awscli-bundle.zip" -o "awscli-bundle.zip"
RUN unzip awscli-bundle.zip
RUN ./awscli-bundle/install -i /usr/local/aws -b /usr/local/bin/aws

# Install elasticsearch

# RUN groupadd -g 1000 elasticsearch && useradd elasticsearch -u 1000 -g 1000
#RUN wget https://artifacts.elastic.co/downloads/elasticsearch/elasticsearch-5.0.1.deb \
#	&& sudo dpkg -i elasticsearch-5.0.1.deb \
#	&& systemctl enable elasticsearch.service \
#	&& mkdir /usr/share/elasticsearch/config/ \ 
#   && echo -e "cluster.name: "docker-cluster" \n\nnetwork.host: 127.0.0.1" > /usr/share/elasticsearch/config/elasticsearch.yml \
#	&& echo -e "rootLogger: INFO,console \n appender: \n console: \n type: console \n layout: \n type: consolePattern \n conversionPattern: "[%d{ISO8601}][%-5p][%-25c] %m%n"" > /usr/share/elasticsearch/config/log4j2.properties
	
#COPY elasticsearch.yml /usr/share/elasticsearch/config/
#COPY log4j2.properties /usr/share/elasticsearch/config/

#ENV PATH=$PATH:/usr/share/elasticsearch/bin

#CMD ["elasticsearch"]

# grab gosu for easy step-down from root
#ENV GOSU_VERSION 1.7
#RUN set -x \
#	&& wget -O /usr/local/bin/gosu "https://github.com/tianon/gosu/releases/download/$GOSU_VERSION/gosu-$(dpkg --print-architecture)" \
#	&& wget -O /usr/local/bin/gosu.asc "https://github.com/tianon/gosu/releases/download/$GOSU_VERSION/gosu-$(dpkg --print-architecture).asc" \
#	&& export GNUPGHOME="$(mktemp -d)" \
#	&& gpg --keyserver ha.pool.sks-keyservers.net --recv-keys B42F6819007F00F88E364FD4036A9C25BF357DD4 \
#	&& gpg --batch --verify /usr/local/bin/gosu.asc /usr/local/bin/gosu \
#	&& rm -r "$GNUPGHOME" /usr/local/bin/gosu.asc \
#	&& chmod +x /usr/local/bin/gosu \
#	&& gosu nobody true
#
# https://artifacts.elastic.co/GPG-KEY-elasticsearch
#RUN apt-key adv --keyserver ha.pool.sks-keyservers.net --recv-keys 46095ACC8548582C1A2699A9D27D666CD88E42B4
#
# https://www.elastic.co/guide/en/elasticsearch/reference/current/setup-repositories.html
# https://www.elastic.co/guide/en/elasticsearch/reference/5.0/deb.html
#RUN set -x \
#	&& apt-get update && apt-get install -y --no-install-recommends apt-transport-https && rm -rf /var/lib/apt/lists/* \
#	&& echo 'deb https://artifacts.elastic.co/packages/5.x/apt stable main' > /etc/apt/sources.list.d/elasticsearch.list
#
#ENV ELASTICSEARCH_VERSION 5.0.1
#
#RUN set -x \
#	&& apt-get update \
#	&& apt-get install -y --no-install-recommends elasticsearch=$ELASTICSEARCH_VERSION \
#	&& rm -rf /var/lib/apt/lists/*
#
#ENV PATH /usr/share/elasticsearch/bin:$PATH

#WORKDIR /usr/share/elasticsearch

#RUN set -ex \
#	&& for path in \
#		./data \
#		./logs \
#		./config \
#		./config/scripts \
#	; do \
#		mkdir -p "$path"; \
#		chown -R kevin:kevin "$path"; \
#	done

#COPY config ./config

#VOLUME /usr/share/elasticsearch/data

#COPY docker-entrypoint.sh /

#EXPOSE 9200 9300
#ENTRYPOINT ["/docker-entrypoint.sh"]
#CMD ["elasticsearch"]



		
#EXPOSE 9200 9300 

# grab gosu for easy step-down from root
ENV GOSU_VERSION 1.10
RUN set -x \
	&& wget -O /usr/local/bin/gosu "https://github.com/tianon/gosu/releases/download/$GOSU_VERSION/gosu-$(dpkg --print-architecture)" \
	&& wget -O /usr/local/bin/gosu.asc "https://github.com/tianon/gosu/releases/download/$GOSU_VERSION/gosu-$(dpkg --print-architecture).asc" \
	&& export GNUPGHOME="$(mktemp -d)" \
	&& gpg --keyserver ha.pool.sks-keyservers.net --recv-keys B42F6819007F00F88E364FD4036A9C25BF357DD4 \
	&& gpg --batch --verify /usr/local/bin/gosu.asc /usr/local/bin/gosu \
	&& rm -rf "$GNUPGHOME" /usr/local/bin/gosu.asc \
	&& chmod +x /usr/local/bin/gosu \
	&& gosu nobody true

RUN set -ex; \
# https://artifacts.elastic.co/GPG-KEY-elasticsearch
	key='46095ACC8548582C1A2699A9D27D666CD88E42B4'; \
	export GNUPGHOME="$(mktemp -d)"; \
	gpg --keyserver ha.pool.sks-keyservers.net --recv-keys "$key"; \
	gpg --export "$key" > /etc/apt/trusted.gpg.d/elastic.gpg; \
	rm -rf "$GNUPGHOME"; \
	apt-key list

# https://www.elastic.co/guide/en/elasticsearch/reference/current/setup-repositories.html
# https://www.elastic.co/guide/en/elasticsearch/reference/5.0/deb.html
RUN set -x \
	&& apt-get update && apt-get install -y --no-install-recommends apt-transport-https && rm -rf /var/lib/apt/lists/* \
	&& echo 'deb https://artifacts.elastic.co/packages/5.x/apt stable main' > /etc/apt/sources.list.d/elasticsearch.list

ENV ELASTICSEARCH_VERSION 5.6.7
ENV ELASTICSEARCH_DEB_VERSION 5.6.7

RUN set -x \
	\
# don't allow the package to install its sysctl file (causes the install to fail)
# Failed to write '262144' to '/proc/sys/vm/max_map_count': Read-only file system
	&& dpkg-divert --rename /usr/lib/sysctl.d/elasticsearch.conf \
	\
	&& apt-get update \
	&& apt-get install -y --no-install-recommends "elasticsearch=$ELASTICSEARCH_DEB_VERSION" \
	&& rm -rf /var/lib/apt/lists/*

ENV PATH /usr/share/elasticsearch/bin:$PATH

WORKDIR /usr/share/elasticsearch

RUN set -ex \
	&& for path in \
		./data \
		./logs \
		./config \
		./config/scripts \
	; do \
		mkdir -p "$path"; \
		chown -R elasticsearch:elasticsearch "$path"; \
	done

COPY config ./config

VOLUME /usr/share/elasticsearch/data

COPY docker-entrypoint.sh /

EXPOSE 9200 9300
ENTRYPOINT ["/docker-entrypoint.sh"]
CMD ["elasticsearch"]

# Install  PHP7.0, composer and selected extensions
#RUN echo "deb http://ppa.launchpad.net/ondrej/php/ubuntu xenial main" > /etc/apt/sources.list.d/ondrej-php.list \
#&& echo "deb http://ppa.launchpad.net/ondrej/php-qa/ubuntu xenial main" > /etc/apt/sources.list.d/ondrej-php-qa.list \ 
#&& apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 4F4EA0AAE5267A6C \
#&& apt-get update \ 
#&& apt-get -y --no-install-recommends install php7.0-cli php7.0-curl  php7.0-gd php7.0-mbstring  php7.0-mongodb php7.0-redis  \ 
#php7.0-json php7.0-mcrypt php7.0-opcache php7.0-xml php7.0-zip curl apt-transport-https \
#&& curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer \ 
#&& apt-get clean \ 
#&& rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* /usr/share/doc/* ~/.composer 
 
# Mongo 

ENV MONGO_MAJOR 3.2 
ENV MONGO_VERSION 3.2.10 
RUN apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv EA312927
RUN echo "deb http://repo.mongodb.org/apt/ubuntu xenial/mongodb-org/3.2 multiverse" | tee /etc/apt/sources.list.d/mongodb-org-3.2.list
RUN set -x \
        && apt-get update \
        && apt-get install -y \
                mongodb-org=$MONGO_VERSION \
                mongodb-org-server=$MONGO_VERSION \
                mongodb-org-shell=$MONGO_VERSION \
                mongodb-org-mongos=$MONGO_VERSION \
                mongodb-org-tools=$MONGO_VERSION \
        && rm -rf /var/lib/apt/lists/* \
        && rm -rf /var/lib/mongodb \
        && mv /etc/mongod.conf /etc/mongod.conf.orig
RUN mkdir -p /data/db /data/configdb \
        && chown -R mongodb:mongodb /data/db /data/configdb
VOLUME /data/db /data/configdb

EXPOSE 27017

# Install Redis.
RUN \
  cd /tmp && \
  wget http://download.redis.io/redis-stable.tar.gz && \
  tar xvzf redis-stable.tar.gz && \
  cd redis-stable && \
  make && \
  make install && \
  cp -f src/redis-sentinel /usr/local/bin && \
  mkdir -p /etc/redis && \
  cp -f *.conf /etc/redis && \
  rm -rf /tmp/redis-stable* && \
  sed -i 's/^\(bind .*\)$/# \1/' /etc/redis/redis.conf && \
  sed -i 's/^\(daemonize .*\)$/# \1/' /etc/redis/redis.conf && \
  sed -i 's/^\(dir .*\)$/# \1\ndir \/data/' /etc/redis/redis.conf && \
  sed -i 's/^\(logfile .*\)$/# \1/' /etc/redis/redis.conf

# Define mountable directories.
VOLUME ["/data"]

# Define working directory.
WORKDIR /data

# Define default command.
CMD ["redis-server", "/etc/redis/redis.conf"]

# Expose ports.
EXPOSE 6379

CMD [ "mongod", "&" ]
CMD ["php", "-a"]